# xmonad-lemonbar

Files associated with my xmonad and lemonbar configurations. These config files allow lemonbar to output workspace and title information from xmonad, which is usually formatted as a non-human-readable string.
The xmonad.hs config file has been adapted from Derek Taylor's
(https://gitlab.com/dwt1).

Requirements (all available in the Arch linux repos):

- [xmonad](https://xmonad.org/)  
- xmonad-utils
- xmonad-contrib
- [lemonbar](https://github.com/LemonBoy/bar) 

To install:
- clone this git repository
- copy the "xmonad.hs" file to ~/.xmonad/
- copy the "bar.sh" to ~/.config/lemonbar/ and make it executable.

Make sure to update the directories in "xmonad.hs" to those relevant to your system. 
Wallpaper can be found [here](https://wallpaperstock.net/boats-wallpapers_w14429.html).

![Desktop_Gitlab](/uploads/8b36d837537b02cf4746ea163a1eba7a/Desktop_Gitlab.png)
