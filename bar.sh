#!/usr/bin/bash

# Define the clock
Clock() {
        DATETIME=$(date "+%a %b %d, %T")

        echo -n "$DATETIME"
}

#Define the Battery
Battery()  {
	BATPERC=$(acpi --battery | cut -d, -f2)
        echo "$BATPERC"
}

while true; do
	w_space=$(head -n 1 /tmp/.xmonad-workspace-log | sed -e 's/<[^<>]*>//g')
        echo "%{l}  $w_space %{c}|$(Clock)| %{r} |Battery: $(Battery)|     "
        sleep 0.2
	
done


